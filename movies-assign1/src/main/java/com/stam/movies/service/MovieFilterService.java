
package com.stam.movies.service;

import java.util.List;
import com.stam.movies.entity.*;
import com.stam.movies.entity.impl.*;

public interface MovieFilterService
{
	List<String> getAllMovies( String theater);
	List<EnglishMovieImpl> getEnglishMovie( String movie );
	List<HinduMovieImpl> getHinduMovie( String movie);
	List<Movie> getMovie( String movie);
}
