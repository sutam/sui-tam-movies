package com.stam.movies.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import com.stam.movies.service.MovieFilterService;
import com.stam.movies.repository.EnglishMovieRepository;
import com.stam.movies.repository.EnglishTheaterRepository;
import com.stam.movies.service.MovieService;
import com.stam.movies.entity.impl.*;
import com.stam.movies.entity.*;
import com.stam.movies.service.MovieListService;
import com.stam.movies.service.TheaterFilterService;
import com.stam.movies.service.TheaterListService;

@Service
@Transactional
public class  MovieServiceImpl implements MovieService {

	@Autowired
	private MovieListService movieListService;
	
	@Autowired
	private MovieFilterService movieFilterService;
	
	@Autowired
	private TheaterListService theaterListService;
	
	@Autowired
	private TheaterFilterService theaterFilterService;	
	
	@Autowired
	private EnglishMovieRepository englishMovieRepository;
	
	@Autowired
	private EnglishTheaterRepository englishTheaterRepository;
	
	@Override
	public List<Movie>   getAllMovies()
	{
	       return movieListService.getAllMovies();
	}
	@Override
	public List<String> getAllTheaters()
	{
		   return theaterListService.getAllTheaters();
	}
	@Override
	public List<Movie> getMovie( String name )
	{
		   return movieFilterService.getMovie(name);
	}
	@Override
	public EnglishMovieImpl getMovie( long id )
	{
		return englishMovieRepository.getMovie(id);
	}
	@Override
	public List<Theater> getTheater ( String name )
	{
		   return theaterFilterService.getTheater(name);
	}
	@Override
	public Theater getTheater ( long id )
	{
		   return englishTheaterRepository.getTheater(id);
	}
	@Override
	public List<String>   getAllMovies( String theater )
	{
		   return movieFilterService.getAllMovies(theater);
	} 
	@Override
	public List<String> getAllTheaters( String movie )
	{
		   return theaterFilterService.getAllTheaters(movie);
	}
	@Override
	public Movie addMovie( Movie movie)
	{
		System.out.println("Added movie name: " + movie.getName() + " movie rating: " + movie.getRating());
		if (movie instanceof EnglishMovieImpl)
		{
			long id =  englishMovieRepository.addMovie((EnglishMovieImpl) movie);
			return englishMovieRepository.getMovie(id);
		}
		if (movie instanceof HinduMovieImpl)
		{
			long id =  englishMovieRepository.addMovie((EnglishMovieImpl) movie);
			return englishMovieRepository.getMovie(id);
		}
		return null;
	}
	@Override
	public EnglishMovieImpl addMovie( EnglishMovieImpl movie)
	{
		//System.out.println("Calling MovieService addMovie");
		System.out.println("Added movie name: " + movie.getName() + " movie rating: " + movie.getRating());
		long id =  englishMovieRepository.addMovie(movie);
		return englishMovieRepository.getMovie(id);
	}
	@Override
	public Theater addTheater( Theater theater)
	{
		//System.out.println("Calling MovieService addTheater");
		System.out.println("Added theater name: " + theater.getName());
		long id =  englishTheaterRepository.addTheater(( EnglishTheater ) theater);
		return englishTheaterRepository.getTheater(id);
	}
}
