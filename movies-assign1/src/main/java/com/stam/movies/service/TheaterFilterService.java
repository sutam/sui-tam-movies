
package com.stam.movies.service;

import java.util.List;

import com.stam.movies.entity.Theater;

public interface TheaterFilterService
{
	List<String> getAllTheaters( String movie);
	List<Theater> getTheater( String theater );
}
