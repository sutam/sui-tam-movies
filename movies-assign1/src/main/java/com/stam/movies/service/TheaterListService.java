
package com.stam.movies.service;

import java.util.List;

import com.stam.movies.entity.Theater;

public interface TheaterListService
{
	List<String> getAllTheaters( );
}
