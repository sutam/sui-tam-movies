package com.stam.movies.service;

import java.util.List;

import com.stam.movies.entity.*;

public interface UserService {
	
	User addUser(User user);
	UserAccount addUserAccount( UserAccount account);
	UserOrderHistory addUserOrder( UserOrderHistory order);
	void updateUser(User user);
	void updateUserAccount( UserAccount account);

	User getUser(long userId);
	UserAccount getUserAccount( long accountId);
	UserOrderHistory getUserOrder( long orderId);
	
	List<UserAccount> getUserAccountForUser ( long userid );
	List<UserOrderHistory>getUserOrderForUser( long userid);
	//List<UserOrderHistory>getUserOrderForUser( User user);
	
	/**
	 * Search user by first or last name, partial searches also performed
	 * @param UserName
	 * @param Password
	 * @return Empty list is returned if none found
	 */
	List<User> getUsers(String username, String password);
	List<UserAccount> getUserAccount( String lastname, String firstname);
	List<UserOrderHistory >getUserOrder(String key);
}
