package com.stam.movies.service;


import java.util.List;

import com.stam.movies.entity.impl.*;
import com.stam.movies.entity.*;

public interface MovieService
{
	EnglishMovieImpl addMovie(EnglishMovieImpl movie);
    Movie addMovie( Movie movie);
	EnglishMovieImpl getMovie(long id);
	Theater addTheater( Theater theater);
	Theater getTheater( long id );
	List<Movie>   getAllMovies();
	List<String> getAllTheaters();
	List<String>   getAllMovies( String theater );
	List<String> getAllTheaters( String movie );
	List<Movie> getMovie( String name );
	List<Theater> getTheater ( String name );
}
