package com.stam.movies.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import com.stam.movies.entity.impl.UserImpl;
import com.stam.movies.repository.UserRepository;
import com.stam.movies.repository.UserAccountRepository;
import com.stam.movies.repository.UserOrderRepository;
import com.stam.movies.service.exception.ErrorCode;
import com.stam.movies.service.exception.InvalidFieldException;
import com.stam.movies.service.exception.TBTFException;
import com.stam.movies.entity.User;
import com.stam.movies.entity.UserAccount;
import com.stam.movies.entity.UserOrderHistory;
import com.stam.movies.service.UserService;

@Service
public class  UserServiceImpl implements UserService {
	
	private static final int MAX_NAME_LENGTH = 45;


	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Autowired
	private UserOrderRepository userOrderRepository;
	
	@Override
	@Transactional//at method level
	public User addUser(User user)
	{
		if(StringUtils.isEmpty(user.getUserName()) || user.getUserName().length()>MAX_NAME_LENGTH){			
			throw new InvalidFieldException("UserName is required and must be less than 46 characters");
		}
		
		if(StringUtils.isEmpty(user.getPassword()) || user.getPassword().length()>MAX_NAME_LENGTH){			
			throw new InvalidFieldException("Password is required and must be less than 46 characters");
		}
		
		long id =  userRepository.addUser(user);
		return getUser(id);
	}
	
	@Override
	@Transactional // at method level
	public UserAccount addUserAccount( UserAccount account)
	{
		if(StringUtils.isEmpty(account.getFirstName()) || account.getFirstName().length()>MAX_NAME_LENGTH){			
			throw new InvalidFieldException("firtname is required and must be less than 46 characters");
		}
		
		if(StringUtils.isEmpty(account.getLastName()) || account.getLastName().length()>MAX_NAME_LENGTH){			
			throw new InvalidFieldException("lastname is required and must be less than 46 characters");
		}
		
		long id =  userAccountRepository.addUserAccount(account);
		return getUserAccount(id);
	}
	@Override
	@Transactional
	public UserOrderHistory addUserOrder( UserOrderHistory order)
	{
		if(StringUtils.isEmpty(order.getOrderHistory()) || order.getOrderHistory().length()>MAX_NAME_LENGTH){			
			throw new InvalidFieldException(" Order history is required and must be less than 46 characters");
		}
		long id =  userOrderRepository.addUserOrder(order);
		return getUserOrder(id);
	}
	@Override
	@Transactional
	public void updateUser(User user) {
		userRepository.update(user);
	}	
	
	@Override
	@Transactional
	public void updateUserAccount(UserAccount account) {
		userAccountRepository.update(account);
	}	
	
	@Override
	@Transactional//at method level
	public User getUser(long userId) {
		return userRepository.getUser(userId);
	}
	
	@Override
	@Transactional//at method level
	public UserAccount getUserAccount(long Id) {
		return userAccountRepository.getUserAccount(Id);
	}

	@Override
	@Transactional//at method level
	public UserOrderHistory getUserOrder(long Id) {
		return userOrderRepository.getUserOrder(Id);
	}
	
	@Override
	@Transactional//at method level
	public List<UserAccount> getUserAccountForUser ( long userid )
	{
		List<UserAccount> returnList = new ArrayList<>();
		returnList = userAccountRepository.search( userid );
		return returnList;
	}
	
	@Override
	@Transactional//at method level
	public List<UserOrderHistory>getUserOrderForUser( long userid)
	{
		List<UserOrderHistory> returnList = new ArrayList<>();
		returnList = userOrderRepository.search( userid );
	    return returnList;
	}
	/**
	 * Search user by first or last name, partial searches also performed
	 * @param UserName
	 * @param Password
	 * @return Empty list is returned if none found
	 */
	@Override
	@Transactional
	public List<User> getUsers(String username, String password)
	{
		List<User> returnList = new ArrayList<>();
		if(StringUtils.isEmpty(username) && StringUtils.isEmpty(password)){
			throw new TBTFException(ErrorCode.MISSING_DATA, "no search parameter provided");	
		}
		else{
			returnList = userRepository.search(username, password);
		}		
		return returnList;
	}
	
	@Override
	@Transactional
	public List<UserOrderHistory >getUserOrder(String key)
	{
		List<UserOrderHistory> returnList = new ArrayList<>();
		if(StringUtils.isEmpty(key) ){
			throw new TBTFException(ErrorCode.MISSING_DATA, "no search parameter provided");	
		}
		else{
			returnList = userOrderRepository.search(key);
		}		
		return returnList;
	}
	@Override
	@Transactional
	public List<UserAccount> getUserAccount(String lastname, String firstname)
	{
		List<UserAccount> returnList = new ArrayList<>();
		if(StringUtils.isEmpty(lastname) && StringUtils.isEmpty(firstname)){
			throw new TBTFException(ErrorCode.MISSING_DATA, "no search parameter provided");	
		}
		else{
			returnList = userAccountRepository.search(lastname, firstname);
		}		
		return returnList;
	}
	
}