package com.stam.movies.service.exception;

public enum ErrorCode {
	INVALID_FIELD,
	MISSING_DATA
}
