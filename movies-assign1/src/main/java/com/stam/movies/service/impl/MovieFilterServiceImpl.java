package com.stam.movies.service.impl;

import java.util.List;
import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stam.movies.service.MovieFilterService;
import com.stam.movies.repository.*;
import com.stam.movies.entity.*;
import com.stam.movies.entity.impl.*;

@Service
@Transactional
public class  MovieFilterServiceImpl implements MovieFilterService {
	
	@Autowired
	private EnglishMovieRepository englishMovieRepository;
	
	@Autowired
	private HinduMovieRepository hinduMovieRepository;

	public List<String>   getAllMovies( String theater )
	{
		   // TO DO:
		   List<String> AllMovies = new ArrayList<String>();
		   
		   return AllMovies;
	}	
	
	public List<EnglishMovieImpl> getEnglishMovie( String name )
	{
	       return englishMovieRepository.searchMovieByName( name );      
	}
	
	public List<HinduMovieImpl> getHinduMovie( String name )
	{
	       return hinduMovieRepository.searchMovieByName( name );      
	}
	
	public List <Movie> getMovie( String name )
	{
		   List<Movie> movies = new ArrayList<>();
		   List<EnglishMovieImpl> englishMovies = getEnglishMovie(name);
		   List<HinduMovieImpl> hinduMovies = getHinduMovie(name);
		   
		   for (int i = 0; i < englishMovies.size(); i++)
			   movies.add( englishMovies.get(i));
		   for (int i = 0; i < hinduMovies.size(); i++)
			   movies.add( hinduMovies.get(i));
		   return movies;
	}	 
}
