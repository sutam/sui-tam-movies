package com.stam.movies.service.impl;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Service;
import com.stam.movies.service.TheaterFilterService;
import com.stam.movies.entity.*;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import com.stam.movies.repository.EnglishTheaterRepository;

@Service
@Transactional
public class  TheaterFilterServiceImpl implements TheaterFilterService {
	@Autowired
	private EnglishTheaterRepository englishTheaterRepository;

	public List<String>   getAllTheaters( String movie )
	{
		   // TO DO:
		   // Get all theaters playing the movie
		   List<String> AllTheaters = new ArrayList<String>();
		   return AllTheaters;
	}	
	
	public List<Theater> getTheater( String name )
	{
		List<EnglishTheater> englishtheaters = englishTheaterRepository.searchTheaterByName( name );      
        List<Theater> theaters = new ArrayList<Theater>();
        for (int i = 0; i < englishtheaters.size(); i++)
        {
     	   theaters.add(englishtheaters.get(i));
        }
        return theaters;
	}

}
