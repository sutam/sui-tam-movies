
package com.stam.movies.service;

import java.util.List;

import com.stam.movies.entity.Movie;

public interface MovieListService
{
	List<Movie> getAllMovies();
}
