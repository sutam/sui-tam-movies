package com.stam.movies.service.impl;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import com.stam.movies.service.MovieListService;
import com.stam.movies.repository.EnglishMovieRepository;
import com.stam.movies.repository.HinduMovieRepository;
import com.stam.movies.entity.*;
import com.stam.movies.entity.impl.*;

@Service
@Transactional
public class  MovieListServiceImpl implements MovieListService {
	@Autowired
	private EnglishMovieRepository englishMovieRepository;
	@Autowired
	private HinduMovieRepository hinduMovieRepository;
	// Unused:
	//        Not included in rest API
	public List<Movie>   getAllMovies()
	{
		List<EnglishMovieImpl> englishMovies =  englishMovieRepository.getMovieForUser();
		List<HinduMovieImpl> hinduMovies = hinduMovieRepository.getMovieForUser();
		List<Movie> allMovies = new ArrayList<Movie>();
		for (int i = 0; i < englishMovies.size(); i++)
		{
			EnglishMovieImpl movie = englishMovies.get(i);
			allMovies.add(movie);	
		}
		for (int i = 0; i < hinduMovies.size(); i++)
		{
			HinduMovieImpl movie = hinduMovies.get(i);
			allMovies.add( movie);
		}
		return allMovies;
	}	
}
