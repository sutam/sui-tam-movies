package com.stam.movies.entity;

import java.util.Date;

public interface UserOrderHistory
{
    String getTransaction();
    void setTransaction(String transaction);
    Date getTransDateTime();
    void setTransDateTime(Date transDateTime);
    String getOrderHistory();
    void setOrderHistory(String orderHistory);
    User getUser();
	void setUser(User user);
	long getId();

}