package com.stam.movies.entity;

import java.util.List;

public interface Movie
{
	String getName();
	long getId();
	void setName( String name );
	String getRating();
	void setRating( String name );
	List<String> getTheaters();
	boolean isPlayingIn( String Theater);
}