package com.stam.movies.entity;

import java.util.List;

public interface Theater
{
	long getId();
	void setName( String name);
	String getName();
	String getCity();
	void setCity( String city );
	String getZipcode();
	String getAddress();
	List<String> getMovies();
	boolean IsPlaying( String movie );
}