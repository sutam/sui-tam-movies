package com.stam.movies.entity.impl;

import java.util.List;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.stam.movies.entity.EnglishMovieSchedule;
import com.stam.movies.entity.*;

@Entity
@Table(name="EnglishMovieSchedule")
public class EnglishMovieScheduleImpl extends AbstractMovieSchedule implements EnglishMovieSchedule, Serializable
{
	@Id
	@Column(name="EnglishMovie_idMovie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idMovie;
	
	@Id
	@Column(name="EnglishTheater_idTheater")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idTheater;
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity=EnglishMovieImpl.class)
	@JoinColumn(name="EnglishMovie_idmovie")
	private EnglishMovie englishmovie;
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity=EnglishTheaterImpl.class)
	@JoinColumn(name="EnglishTheater_idtheater")
	private EnglishTheater englishtheater;
	
	public long getidMovie()
	{
		return idMovie;
	}
	
	public long getidTheater()
	{
		return idTheater;
	}
}
