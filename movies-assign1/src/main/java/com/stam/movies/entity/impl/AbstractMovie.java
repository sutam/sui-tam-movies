package com.stam.movies.entity.impl;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.stam.movies.entity.Theater;
import java.util.List;

@MappedSuperclass
public abstract class AbstractMovie {
	@Id
	@Column(name="idMovie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="rating")
	private String rating;
	
	public String getName()
	{
		return name;
	}
	
	public void setName (String name)
	{
		this.name = name;
	}
	
	public long getId()
	{
		return id;
	}
	public String getRating()
	{
		return rating;
	}
	public void setRating( String rating )
	{
		this.rating = rating;
	}
	
	public List<String> getTheaters()
	{
		return null;
	}
	public boolean isPlayingIn( String Theater)
	{
		return true;
	}
}