package com.stam.movies.entity.impl;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.stam.movies.entity.EnglishMovie;
import com.stam.movies.entity.EnglishTheater;
import com.stam.movies.entity.EnglishMovieSchedule;

@Entity
@Table(name="EnglishMovie")
public class EnglishMovieImpl extends AbstractMovie implements EnglishMovie
{
	@OneToMany(mappedBy = "englishmovie", targetEntity=EnglishMovieScheduleImpl.class, cascade=CascadeType.ALL)	
	private List<EnglishMovieSchedule>  scheduleForMovie;
	
	//@ManyToMany(targetEntity=EnglishTheaterImpl.class, fetch=FetchType.EAGER)
	//@JoinTable(name="EnglishMovieSchedule", joinColumns = { @JoinColumn(name = "EnglishMovie_idMovie", nullable = false) }, 
	//		inverseJoinColumns = { @JoinColumn(name = "EnglishTheater_idTheater", nullable = false) })
	//private List<EnglishTheater> english_theaters;
	
}
