package com.stam.movies.entity.impl;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.stam.movies.entity.User;
import com.stam.movies.entity.UserOrderHistory;

/**
 * Order History
 *
 */
@Entity
@Table(name="users_audit_history")
public class UserOrderHistoryImpl implements UserOrderHistory {

	@Id
	@Column(name="idusers_audit_history")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="transaction")
	private String transaction;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_date_time")
	private Date trans_datetime;
	
	@Column(name="order_history")
	private String order_history;
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity=UserImpl.class)
	@JoinColumn(name="users_idusers")
	private User user;
	
	@Override
	public String getTransaction()
	{
		return transaction;
	}
	
	@Override
	public void setTransaction(String transaction)
	{
		this.transaction = transaction;
	}
	
	@Override
	public Date getTransDateTime() {
		return trans_datetime;
	}
	
	@Override
	public void setTransDateTime(Date transDateTime) {
		trans_datetime = transDateTime;
	}

	@Override
	public String getOrderHistory() {
		return order_history;
	}

	@Override
	public void setOrderHistory(String orderHistory) {
		this.order_history = orderHistory;
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public long getId() {
		return id;
	}
}
