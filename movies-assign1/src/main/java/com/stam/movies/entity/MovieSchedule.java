package com.stam.movies.entity;

import java.util.Date;

public interface MovieSchedule
{
	Date getShowtime();
	long getidMovie();
	long getidTheater();
}