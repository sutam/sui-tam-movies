package com.stam.movies.entity;


public interface HinduMovieSchedule extends MovieSchedule
{
	long getidMovie();
	long getidTheater();
}