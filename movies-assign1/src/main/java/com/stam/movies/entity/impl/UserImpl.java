package com.stam.movies.entity.impl;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import java.util.List;

import com.stam.movies.entity.UserAccount;
import com.stam.movies.entity.UserOrderHistory;
import com.stam.movies.entity.impl.UserOrderHistoryImpl;
import com.stam.movies.entity.User;

@Entity
@Table(name="User")
public class UserImpl implements User {

	@Id
	@Column(name="idUser")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name ="user_name")
	private String UserName;
	
	@Column(name ="password")
	private String Password;
	
	@OneToOne(targetEntity=UserAccountImpl.class, optional=true, cascade=CascadeType.ALL, mappedBy="user")
	private UserAccount account;
	
	@OneToMany(mappedBy = "user", targetEntity=UserOrderHistoryImpl.class, cascade=CascadeType.ALL)	
    private List<UserOrderHistory> userOrderHistory;
	
	public long getId()
	{
		return id;
	}
	public String getUserName()
	{
		return UserName;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setUserName(String name)
	{
		this.UserName = name;
	}
	public void setPassword(String password)
	{
		this.Password = password;
	}
}
