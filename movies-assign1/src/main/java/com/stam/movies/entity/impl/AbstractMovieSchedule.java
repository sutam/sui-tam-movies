package com.stam.movies.entity.impl;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Date;

@MappedSuperclass
public class AbstractMovieSchedule
{
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "show_time", columnDefinition="DATETIME")
	private Date show_time;
	
	public Date getShowtime()
	{
		return show_time;
	}
	
}