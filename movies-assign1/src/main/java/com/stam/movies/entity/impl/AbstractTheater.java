package com.stam.movies.entity.impl;

import com.stam.movies.entity.Movie;
import java.util.List;
import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@MappedSuperclass
public abstract class AbstractTheater {
	
	@Id
	@Column(name="idTheater")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="category")
	private String category;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="zipcode")
	private String zipcode;
	
	@Column(name="address")
	private String address;
	
	//List<LocalDateTime> schedule;
	
	//List <Movie> moviesPlaying;
	
	public long getId()
	{
		return id;
	}
	public String getName()
	{
		return this.name;
	}
	public void setName ( String name )
	{
		this.name = name;
	}
	public String getCategory( String name)
	{
		return category;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity( String city )
	{
		this.city = city;
	}
	public String getState()
	{
		return state;
	}
	public String getZipcode()
	{
		return zipcode;
	}
	public String getAddress()
	{
		return address;
	}
	public List<String> getMovies()
	{
		return null;
	}
	public boolean IsPlaying( String movie )
	{
		return true;
	}
}