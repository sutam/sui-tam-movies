package com.stam.movies.entity.impl;

import com.stam.movies.entity.HinduMovie;
import com.stam.movies.entity.HinduTheater;
import com.stam.movies.entity.MovieSchedule;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="HinduTheater")
public class HinduTheaterImpl extends AbstractTheater implements HinduTheater
{
	
	@Column(name="LocalPromotionChannel")
	String LocalPromotionChannel;
	
	//@ManyToMany(fetch = FetchType.LAZY, mappedBy = "hindu_theaters", targetEntity=HinduMovieImpl.class)	
	//private List<HinduMovie> hindu_movies;
	
	@OneToMany(mappedBy = "hindutheater", targetEntity=HinduMovieScheduleImpl.class, cascade=CascadeType.ALL)	
	private List<MovieSchedule>  scheduleForTheater;
	
	public String getLocalPromotionChannel()
	{
		return LocalPromotionChannel;
	}
}