package com.stam.movies.entity;

public interface User {

	long getId();
	String getUserName();
	String getPassword();
	void setUserName(String name);
	void setPassword(String password);
}
