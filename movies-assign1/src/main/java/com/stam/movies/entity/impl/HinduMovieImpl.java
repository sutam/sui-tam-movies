package com.stam.movies.entity.impl;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.stam.movies.entity.HinduMovie;
import com.stam.movies.entity.HinduMovieSchedule;
import com.stam.movies.entity.impl.HinduMovieScheduleImpl;

import java.util.List;

@Entity
@Table(name="HinduMovie")
public class HinduMovieImpl extends AbstractMovie implements HinduMovie
{
	@Column(name="subtitles")
	String subtitles;
	
	@OneToMany(mappedBy = "hindumovie", targetEntity=HinduMovieScheduleImpl.class, cascade=CascadeType.ALL)	
	private List<HinduMovieSchedule>  scheduleForMovie;
	
	//@ManyToMany(targetEntity=HinduTheaterImpl.class, fetch=FetchType.EAGER)
	//@JoinTable(name="HinduMovieSchedule", joinColumns = { @JoinColumn(name = "HinduMovie_idMovie", nullable = false) }, 
	//		inverseJoinColumns = { @JoinColumn(name = "HinduTheater_idTheater", nullable = false) })
	//private List<HinduTheater> hindu_theaters;
	public String getSubtitles()
	{
		return subtitles;
	}
}