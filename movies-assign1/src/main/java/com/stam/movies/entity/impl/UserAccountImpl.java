package com.stam.movies.entity.impl;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import java.util.List;

import com.stam.movies.entity.impl.UserImpl;
import com.stam.movies.entity.UserAccount;
import com.stam.movies.entity.User;

@Entity
@Table(name="UserAccount")
public class UserAccountImpl implements UserAccount {
	
	@Id
	@Column(name="idAccount")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name ="first_name")
	private String first_name;
	
	@Column(name ="last_name")
	private String last_name;
	
	@Column(name ="email_address")
	private String email_address;
	
	@Column(name="street_address")
	private String street_address;
	
	@Column(name="city")
	private String city;
	
	@Column( name="state")
	private String state;
	
	@Column( name="zip")
	private String zip;
	
	@Column( name = "status")
	private String status;
	
	@Column( name ="membership")
	private String membership;
	
	//@Column( name = "users_idusers")
	//private long userid;
	
	@OneToOne(fetch = FetchType.LAZY, targetEntity=UserImpl.class)
	@JoinColumn(name="users_idusers")	
	private User user;
	@Override
	public String getFirstName()
	{
		return first_name;
	}
	@Override
	public String getLastName()
	{
		return last_name;
	}
	@Override
	public String getEmailAddress()
	{
		return email_address;
	}
	@Override
	public String getStreetAddress()
	{
		return street_address;
	}
	@Override
	public String getCity()
	{
		return city;
	}
	@Override
	public String getState()
	{
		return state;
	}
	@Override
	public String getZip()
	{
		return zip;
	}
	@Override
	public String getStatus()
	{
		return status;
	}
	@Override
	public String getMembership()
	{
		return membership;
	}
	@Override
	public long getId() {
		return id;
	}
	
	//@Override
	//public long getUserId() {
	//	return this.userid;
	//}

	@Override
	public User getUser() {
		return user;
	}
	@Override
	public void setUser(User user) {
		this.user = user;
	}
	//@Override
	//public void setUserId ( long userid) {
	//	this.userid = userid;
	//}
	@Override 
	public void setFirstName ( String firstname )
	{
		this.first_name = firstname;
	}
	@Override
	public void setLastName ( String lastname )
	{
		this.last_name = lastname;
	}
	
}
