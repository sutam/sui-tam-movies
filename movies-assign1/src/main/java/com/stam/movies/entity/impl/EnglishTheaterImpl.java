package com.stam.movies.entity.impl;

import com.stam.movies.entity.EnglishTheater;
import com.stam.movies.entity.EnglishMovie;
import com.stam.movies.entity.MovieSchedule;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="EnglishTheater")
public class EnglishTheaterImpl extends AbstractTheater implements EnglishTheater
{
	@OneToMany(mappedBy = "englishtheater", targetEntity=EnglishMovieScheduleImpl.class, cascade=CascadeType.ALL)	
	private List<MovieSchedule>  scheduleForTheater;
	
	//@ManyToMany(fetch = FetchType.LAZY, mappedBy = "english_theaters", targetEntity=EnglishMovieImpl.class)	
	//private List<EnglishMovie> english_movies;
	
}