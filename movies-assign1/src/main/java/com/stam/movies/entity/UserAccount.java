package com.stam.movies.entity;

public interface UserAccount {

	String getFirstName();
	String getLastName();
	String getEmailAddress();
	String getStreetAddress();
	String getCity();
	String getState();
	String getZip();
	String getStatus();
	String getMembership();
	//long getUserId();
	//void setUserId( long id );
	void setFirstName( String firstname );
	void setLastName( String lastname );
	long getId();
	User getUser() ;
	void setUser( User user);
	
}
