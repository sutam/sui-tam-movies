package com.stam.movies.entity.impl;

import java.util.List;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.stam.movies.entity.HinduMovieSchedule;
import com.stam.movies.entity.EnglishMovie;
import com.stam.movies.entity.EnglishTheater;
import com.stam.movies.entity.HinduMovie;

@Entity
@Table(name="HinduMovieSchedule")
public class HinduMovieScheduleImpl extends AbstractMovieSchedule implements HinduMovieSchedule, Serializable
{
	@Id
	@Column(name="HinduMovie_idMovie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idMovie;
	
	@Id
	@Column(name="HinduTheater_idTheater")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idTheater;
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity=HinduMovieImpl.class)
	@JoinColumn(name="HinduMovie_idmovie")
	private EnglishMovie hindumovie;
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity=HinduTheaterImpl.class)
	@JoinColumn(name="HinduTheater_idtheater")
	private EnglishTheater hindutheater;
	
	public long getidMovie()
	{
		return idMovie;
	}
	
	public long getidTheater()
	{
		return idTheater;
	}
}