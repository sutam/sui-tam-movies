package com.stam.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stam.movies.entity.Theater;
import com.stam.movies.entity.impl.EnglishTheaterImpl;
import com.stam.movies.http.entity.HttpTheater;
import com.stam.movies.service.MovieService;
import com.stam.movies.service.exception.TBTFException;

@Path("/Theater")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class TheaterResource {
private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieService movieService;
	
	@POST
	@Path("/")
	public Response createTheater(HttpTheater newTheater){
		Theater theaterToCreate = convert(newTheater);
		Theater addedTheater = movieService.addTheater(theaterToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+addedTheater.getId()).entity(new HttpTheater(addedTheater)).build();
	}	

	@GET
	@Path("/{theater}")	
	public HttpTheater getMovieById(@PathParam("movieId") long theaterId){
		logger.info("getting theater by id:"+theaterId);
		Theater theater = movieService.getTheater(theaterId);	
		return new HttpTheater(theater);
	}
	
	@GET
	@Path("/")
	@Wrapped(element="Theater")
	public List<HttpTheater> getMovieSearch(@QueryParam("Name") String name) throws TBTFException{
		logger.info("user search Name="+name);
		List<Theater> found = movieService.getTheater(name);
		List<HttpTheater> returnList = new ArrayList<>(found.size());
		for(Theater theater:found){
			returnList.add(new HttpTheater(theater));
		}
		return returnList;
	}
	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @return
	 */
	private Theater convert(HttpTheater httptheater) {
		Theater theater = new EnglishTheaterImpl();
		theater.setName(httptheater.name);
		return theater;
	}	
}