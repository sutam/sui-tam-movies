package com.stam.movies.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.stam.movies.entity.Movie;

/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 * 
 *
 */
@XmlRootElement(name = "Movie")
public class HttpMovie {
	
	@XmlElement
	public long id;
	
	@XmlElement
	public String name;
	
	@XmlElement
	public String rating;
	
	//required by framework
	protected HttpMovie() {}

	public HttpMovie(Movie movie) {
		this.id=movie.getId();
		this.name=movie.getName();
		this.rating=movie.getRating();
	}
	
}