package com.stam.movies.http.entity;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.stam.movies.entity.UserOrderHistory;

/**
 *  HttpUserOrder
 *
 */
@XmlRootElement(name = "userorder")
public class HttpUserOrder {
	
	@XmlElement
	public long id;
	
	@XmlElement
	public String order_history;
	
	@XmlElement
	public String transaction;
	
	@XmlElement
	public Date trans_datetime;
	
	//required by framework
	protected HttpUserOrder() {}

	public HttpUserOrder(UserOrderHistory order) {
		this.id=order.getId();
		this.order_history=order.getOrderHistory();
		this.transaction=order.getTransaction();
		this.trans_datetime = order.getTransDateTime();
	}
	
}
