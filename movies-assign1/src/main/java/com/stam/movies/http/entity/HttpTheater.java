package com.stam.movies.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.stam.movies.entity.Theater;

/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 * 
 *
 */
@XmlRootElement(name = "Theater")
public class HttpTheater {
	
	@XmlElement
	public long id;
	
	@XmlElement
	public String name;
	
	@XmlElement
	public String rating;
	
	//required by framework
	protected HttpTheater() {}

	public HttpTheater(Theater theater) {
		this.id=theater.getId();
		this.name=theater.getName();
		this.rating=theater.getCity();
	}
	
}