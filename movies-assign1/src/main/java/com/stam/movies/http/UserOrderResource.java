package com.stam.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stam.movies.entity.UserOrderHistory;
import com.stam.movies.entity.impl.UserOrderHistoryImpl;
import com.stam.movies.http.entity.HttpUserOrder;
import com.stam.movies.service.UserService;
import com.stam.movies.service.exception.TBTFException;

@Path("/userorder")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserOrderResource {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserService userService;
	
	@POST
	@Path("/")
	public Response createOrder(HttpUserOrder newOrder){
		UserOrderHistory orderToCreate = convert(newOrder);
		UserOrderHistory addedOrder = userService.addUserOrder(orderToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+addedOrder.getId()).entity(new HttpUserOrder(addedOrder)).build();
	}	

	@GET
	@Path("/{orderId}")	
	public HttpUserOrder getOrderById(@PathParam("accountId") long userId){
		logger.info("getting order by id:"+userId);
		UserOrderHistory order = userService.getUserOrder(userId);	
		return new HttpUserOrder(order);
	}
	
	@GET
	@Path("/")
	@Wrapped(element="orderaccount")
	public List<HttpUserOrder> getOrderSearch(@QueryParam("key") String key) throws TBTFException{
		logger.info("user search key="+key);
		List<UserOrderHistory> found = userService.getUserOrder(key);
		List<HttpUserOrder> returnList = new ArrayList<>(found.size());
		for(UserOrderHistory order:found){
			returnList.add(new HttpUserOrder(order));
		}
		return returnList;
	}
	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @return
	 */
	private UserOrderHistory convert(HttpUserOrder UserOrder) {
		UserOrderHistoryImpl order = new UserOrderHistoryImpl();
		order.setOrderHistory(UserOrder.order_history);
		order.setTransaction(UserOrder.transaction);
		return order;
	}	
}
