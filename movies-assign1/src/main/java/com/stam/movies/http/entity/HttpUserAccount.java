package com.stam.movies.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.stam.movies.entity.UserAccount;

/**
 *  HttpUserAccount
 *
 */
@XmlRootElement(name = "useraccount")
public class HttpUserAccount {
	
	@XmlElement
	public long id;
	
	@XmlElement
	public String Firstname;
	
	@XmlElement
	public String Lastname;
	
	//required by framework
	protected HttpUserAccount() {}

	public HttpUserAccount(UserAccount account) {
		this.id=account.getId();
		this.Firstname=account.getFirstName();
		this.Lastname=account.getLastName();
	}
	
}
