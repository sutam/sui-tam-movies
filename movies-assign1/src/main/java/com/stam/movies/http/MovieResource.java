package com.stam.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stam.movies.entity.Movie;
import com.stam.movies.entity.impl.EnglishMovieImpl;
import com.stam.movies.http.entity.HttpMovie;
import com.stam.movies.service.MovieService;
import com.stam.movies.service.exception.TBTFException;

@Path("/Movie")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class MovieResource {
private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieService movieService;
	
	@POST
	@Path("/")
	public Response createMovie(HttpMovie newMovie){
		Movie movieToCreate = convert(newMovie);
		Movie addedMovie = movieService.addMovie(movieToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+addedMovie.getId()).entity(new HttpMovie(addedMovie)).build();
	}	

	@GET
	@Path("/{movie}")	
	public HttpMovie getMovieById(@PathParam("movieId") long movieId){
		logger.info("getting movie by id:"+movieId);
		Movie movie = movieService.getMovie(movieId);	
		return new HttpMovie(movie);
	}
	
	@GET
	@Path("/")
	@Wrapped(element="Movie")
	public List<HttpMovie> getMovieSearch(@QueryParam("Name") String name) throws TBTFException{
		logger.info("user search Name="+name);
		List<Movie> found = movieService.getMovie(name);
		List<HttpMovie> returnList = new ArrayList<>(found.size());
		for(Movie movie:found){
			returnList.add(new HttpMovie(movie));
		}
		return returnList;
	}
	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @return
	 */
	private Movie convert(HttpMovie httpMovie) {
		Movie movie = new EnglishMovieImpl();
		movie.setName(httpMovie.name);
		movie.setRating(httpMovie.rating);
		return movie;
	}	
}