package com.stam.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stam.movies.entity.User;
import com.stam.movies.entity.impl.UserImpl;
import com.stam.movies.http.entity.HttpUser;
import com.stam.movies.service.UserService;
import com.stam.movies.service.exception.TBTFException;

@Path("/users")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserResource {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserService userService;
	
	@POST
	@Path("/")
	public Response createUser(HttpUser newUser){
		User userToCreate = convert(newUser);
		User addedUser = userService.addUser(userToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+addedUser.getId()).entity(new HttpUser(addedUser)).build();
	}	

	@GET
	@Path("/{userId}")	
	public HttpUser getUserById(@PathParam("userId") long userId){
		logger.info("getting user by id:"+userId);
		User user = userService.getUser(userId);	
		return new HttpUser(user);
	}
	
	@GET
	@Path("/")
	@Wrapped(element="users")
	public List<HttpUser> getUserSearch(@QueryParam("Username") String username, @QueryParam("Password") String password) throws TBTFException{
		logger.info("user search userName="+username+" Password="+password);
		List<User> found = userService.getUsers(username, password);
		List<HttpUser> returnList = new ArrayList<>(found.size());
		for(User user:found){
			returnList.add(new HttpUser(user));
		}
		return returnList;
	}
	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @param newUser
	 * @return
	 */
	private User convert(HttpUser httpUser) {
		UserImpl user = new UserImpl();
		user.setUserName(httpUser.Username);
		user.setPassword(httpUser.Password);
		return user;
	}	
}
