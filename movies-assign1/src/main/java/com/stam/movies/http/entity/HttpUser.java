package com.stam.movies.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.stam.movies.entity.User;

/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 * 
 * @author rahul
 *
 */
@XmlRootElement(name = "user")
public class HttpUser {
	
	@XmlElement
	public long id;
	
	@XmlElement
	public String Username;
	
	@XmlElement
	public String Password;
	
	//required by framework
	protected HttpUser() {}

	public HttpUser(User user) {
		this.id=user.getId();
		this.Username=user.getUserName();
		this.Password=user.getPassword();
	}
	
}
