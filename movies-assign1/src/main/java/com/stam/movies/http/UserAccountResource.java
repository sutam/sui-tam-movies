package com.stam.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stam.movies.entity.UserAccount;
import com.stam.movies.entity.impl.UserAccountImpl;
import com.stam.movies.http.entity.HttpUserAccount;
import com.stam.movies.service.UserService;
import com.stam.movies.service.exception.TBTFException;

@Path("/useraccount")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserAccountResource {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserService userService;
	
	@POST
	@Path("/")
	public Response createAccount(HttpUserAccount newAccount){
		UserAccount accountToCreate = convert(newAccount);
		UserAccount addedAccount = userService.addUserAccount(accountToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+addedAccount.getId()).entity(new HttpUserAccount(addedAccount)).build();
	}	

	@GET
	@Path("/{accountId}")	
	public HttpUserAccount getAccountById(@PathParam("accountId") long userId){
		logger.info("getting account by id:"+userId);
		UserAccount account = userService.getUserAccount(userId);	
		return new HttpUserAccount(account);
	}
	
	@GET
	@Path("/")
	@Wrapped(element="useraccount")
	public List<HttpUserAccount> getAccountSearch(@QueryParam("Firstname") String firstname, @QueryParam("Lastname") String lastname) throws TBTFException{
		logger.info("user search firstName="+firstname+" Password="+lastname);
		List<UserAccount> found = userService.getUserAccount(firstname, lastname);
		List<HttpUserAccount> returnList = new ArrayList<>(found.size());
		for(UserAccount account:found){
			returnList.add(new HttpUserAccount(account));
		}
		return returnList;
	}
	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @return
	 */
	private UserAccount convert(HttpUserAccount httpUserAccount) {
		UserAccountImpl account = new UserAccountImpl();
		account.setFirstName(httpUserAccount.Firstname);
		account.setLastName(httpUserAccount.Lastname);
		//account.setUserId(httpUserAccount.id);
		return account;
	}	
}
