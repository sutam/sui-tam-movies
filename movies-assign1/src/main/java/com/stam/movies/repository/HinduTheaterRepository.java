package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.HinduTheater;
import com.stam.movies.entity.User;

public interface HinduTheaterRepository {

	long addTheater(HinduTheater theater);
	
	HinduTheater getTheater(long theaterId);
	
	List<HinduTheater> getTheaterForUser();
	
	List<HinduTheater> searchTheaterByName(String movieName );
	
}
