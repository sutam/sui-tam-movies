package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.UserOrderHistory;

public interface UserOrderRepository {
	
	/**
	 * 
	 * @return the id of the newly added order
	 */
	long addUserOrder(UserOrderHistory order);
	
	UserOrderHistory getUserOrder(long orderId);
	
	List<UserOrderHistory> search(String key );
	
	List<UserOrderHistory> search (long userId);

	void update(UserOrderHistory  order);

}
