package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;

import com.stam.movies.entity.EnglishTheater;
import com.stam.movies.entity.impl.EnglishTheaterImpl;
import com.stam.movies.repository.EnglishTheaterRepository;

@Repository
public class EnglishTheaterRepositoryImpl implements EnglishTheaterRepository{

	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param theater
	 * @return id of the newly created theater
	 */
	@Override
	public long addTheater(EnglishTheater theater)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(theater);
	}
	
	public EnglishTheater getTheater(long theaterId)
	{
		return (EnglishTheater) this.sessionFactory.getCurrentSession().get(EnglishTheaterImpl.class, theaterId);
	}
	
	@SuppressWarnings("unchecked")
	public List<EnglishTheater> getTheaterForUser()
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(EnglishTheater.class);
		List<EnglishTheater> searchResult = crit.list();		
		return searchResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<EnglishTheater> searchTheaterByName ( String theaterName )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(EnglishTheater.class);
		if(!StringUtils.isEmpty(theaterName)){
			crit.add(Restrictions.like("name", "%" + theaterName+"%"));
		}
		List<EnglishTheater> searchResult = crit.list();		
		return searchResult;
	}
	
}
