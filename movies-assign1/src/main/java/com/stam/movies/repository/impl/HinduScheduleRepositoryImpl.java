package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;
import java.util.ArrayList;

import com.stam.movies.entity.HinduMovie;
import com.stam.movies.entity.HinduMovieSchedule;
import com.stam.movies.repository.HinduScheduleRepository;
import com.stam.movies.entity.User;

@Repository
public class HinduScheduleRepositoryImpl implements HinduScheduleRepository{
	
	@Autowired
    private SessionFactory sessionFactory;
	
	public long addSchedule(HinduMovieSchedule schedule)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(schedule);
	}
	
	@SuppressWarnings("unchecked")
	public List<HinduMovieSchedule> searchScheduleByName(long theater, long movie )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(HinduMovieSchedule.class);
		crit.add(Restrictions.like("HinduMovie_idMovie", "%" + movie +"%"));
		List<HinduMovieSchedule> searchResult = crit.list();	
		List<HinduMovieSchedule> result =  new ArrayList<>();
		for (int i = 0; i < searchResult.size(); i++)
		{
			HinduMovieSchedule item = searchResult.get(i);
			if ( item.getidTheater() == theater )
				result.add(item);
		}
		return result;
	}
	
}
