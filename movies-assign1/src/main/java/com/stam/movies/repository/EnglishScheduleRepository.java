package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.EnglishMovieSchedule;
import com.stam.movies.entity.User;

public interface EnglishScheduleRepository {

	long addSchedule(EnglishMovieSchedule theater);
	
	List<EnglishMovieSchedule> searchScheduleByName(long theater, long movie );
	
}
