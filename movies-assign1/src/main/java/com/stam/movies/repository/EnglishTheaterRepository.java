package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.EnglishTheater;
import com.stam.movies.entity.User;

public interface EnglishTheaterRepository {

	long addTheater(EnglishTheater theater);
	
	EnglishTheater getTheater(long theaterId);
	
	List<EnglishTheater> getTheaterForUser();
	
	List<EnglishTheater> searchTheaterByName(String movieName );
	
}
