package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.impl.HinduMovieImpl;

public interface HinduMovieRepository {

	long addMovie(HinduMovieImpl movie);
	
	HinduMovieImpl getMovie(long movieId);
	
	List<HinduMovieImpl> getMovieForUser();
	
	List<HinduMovieImpl> searchMovieByName( String Name );
}
