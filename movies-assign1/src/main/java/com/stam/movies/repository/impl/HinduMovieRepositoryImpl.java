package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;

import com.stam.movies.entity.HinduMovie;
import com.stam.movies.entity.impl.HinduMovieImpl;
import com.stam.movies.repository.HinduMovieRepository;

@Repository
public class HinduMovieRepositoryImpl implements HinduMovieRepository{

	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param theater
	 * @return id of the newly created movie
	 */
	@Override
	public long addMovie(HinduMovieImpl movie)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(movie);
	}
	
	public HinduMovieImpl getMovie(long theaterId)
	{
		return (HinduMovieImpl) this.sessionFactory.getCurrentSession().get(HinduMovieImpl.class, theaterId);
	}
	
	@SuppressWarnings("unchecked")
	public List<HinduMovieImpl> getMovieForUser()
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(HinduMovie.class);
		List<HinduMovieImpl> searchResult = crit.list();		
		return searchResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<HinduMovieImpl> searchMovieByName ( String movieName )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(HinduMovie.class);
		if(!StringUtils.isEmpty(movieName)){
			crit.add(Restrictions.like("name", "%" + movieName+"%"));
		}
		List<HinduMovieImpl> searchResult = crit.list();		
		return searchResult;
	}
	
}
