package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.UserAccount;

public interface UserAccountRepository {
	
	/**
	 * 
	 * @param user
	 * @return the id of the newly added account
	 */
	long addUserAccount(UserAccount account);
	
	UserAccount getUserAccount(long accountId);
	
	List<UserAccount> search(String firstname, String lastname);

	List<UserAccount> search ( long userid );
	
	void update(UserAccount account);

}
