package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.User;

public interface UserRepository {
	
	/**
	 * 
	 * @param user
	 * @return the id of the newly added user
	 */
	long addUser(User user);
	
	User getUser(long userId);
	
	List<User> search(String Username, String Password);

	void update(User user);

}
