package com.stam.movies.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.stam.movies.repository.UserAccountRepository;
import com.stam.movies.entity.UserAccount;
import com.stam.movies.entity.impl.UserAccountImpl;

@Repository
public class UserAccountRepositoryImpl implements UserAccountRepository {
	
	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param account
	 * @return the id of the newly added account
	 */
	@Override
	public long addUserAccount(UserAccount account)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(account);
	}
	
	@Override
	public UserAccount getUserAccount(long accountId)
	{
		return (UserAccount) this.sessionFactory.getCurrentSession().get(UserAccountImpl.class, accountId);
	}
	
	@SuppressWarnings("unchecked")
	public List<UserAccount> search(String firstname, String lastname)
	{

		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class);
		if(!StringUtils.isEmpty(firstname)){
			crit.add(Restrictions.like("first_name", "%"+firstname+"%"));
		}
		if(!StringUtils.isEmpty(lastname)){
			crit.add(Restrictions.like("last_name", "%"+lastname+"%"));
		}
		List<UserAccount> searchResult = crit.list();		
		return searchResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserAccount> search( long userid )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class);
		crit.add(Restrictions.like("User_idUser", Long.toString(userid)));
		List<UserAccount> searchResult = crit.list();		
		return searchResult;
	}


	@Override
	public void update(UserAccount account)
	{
		this.sessionFactory.getCurrentSession().update(account);	
	}

}
