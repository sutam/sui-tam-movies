package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.impl.EnglishMovieImpl;

public interface EnglishMovieRepository {
	
	long addMovie(EnglishMovieImpl movie);
	
	EnglishMovieImpl getMovie(long movieId);
	
	List<EnglishMovieImpl> getMovieForUser();
	
	List<EnglishMovieImpl> searchMovieByName( String Name );
}
