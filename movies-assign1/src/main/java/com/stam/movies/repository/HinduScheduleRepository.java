package com.stam.movies.repository;

import java.util.List;

import com.stam.movies.entity.HinduMovieSchedule;
import com.stam.movies.entity.User;

public interface HinduScheduleRepository {

	long addSchedule(HinduMovieSchedule theater);
	
	List<HinduMovieSchedule> searchScheduleByName(long theater, long movie );
	
}
