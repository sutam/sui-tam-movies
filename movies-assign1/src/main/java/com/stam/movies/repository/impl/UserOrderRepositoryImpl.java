package com.stam.movies.repository.impl;

import java.util.List;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.stam.movies.repository.UserOrderRepository;
import com.stam.movies.entity.UserOrderHistory;
import com.stam.movies.entity.impl.UserOrderHistoryImpl;

@Repository
public class UserOrderRepositoryImpl implements UserOrderRepository {
	
	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param account
	 * @return the id of the newly added account
	 */
	@Override
	public long addUserOrder(UserOrderHistory order)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(order);
	}
	
	@Override
	public UserOrderHistory getUserOrder(long orderId)
	{
		return (UserOrderHistory) this.sessionFactory.getCurrentSession().get(UserOrderHistoryImpl.class, orderId);
	}
	
	@SuppressWarnings("unchecked")
	public List<UserOrderHistory> search(String key )
	{

		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(UserOrderHistory.class);
		if(!StringUtils.isEmpty(key)){
			crit.add(Restrictions.like("order_history", "%"+key+"%"));
		}
		
		List<UserOrderHistory> searchResult = crit.list();		
		return searchResult;
	}

	@SuppressWarnings("unchecked")
	public List<UserOrderHistory> search( long userId )
	{

		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(UserOrderHistory.class);
		crit.add(Restrictions.like("User_idUser", Long.toString(userId)));
		List<UserOrderHistory> searchResult = crit.list();		
		return searchResult;
	}
	
	@Override
	public void update(UserOrderHistory order)
	{
		this.sessionFactory.getCurrentSession().update(order);	
	}

}
