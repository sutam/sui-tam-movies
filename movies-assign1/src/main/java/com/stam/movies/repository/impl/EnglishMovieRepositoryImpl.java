package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;

import com.stam.movies.entity.EnglishMovie;
import com.stam.movies.entity.impl.EnglishMovieImpl;
import com.stam.movies.repository.EnglishMovieRepository;

@Repository
public class EnglishMovieRepositoryImpl implements EnglishMovieRepository{

	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param theater
	 * @return id of the newly created movie
	 */
	@Override
	public long addMovie(EnglishMovieImpl movie)
	{
		return (long) this.sessionFactory.getCurrentSession().save(movie);
	}
	
	public EnglishMovieImpl getMovie(long theaterId)
	{
		return (EnglishMovieImpl) this.sessionFactory.getCurrentSession().get(EnglishMovieImpl.class, theaterId);
	}
	
	@SuppressWarnings("unchecked")
	public List<EnglishMovieImpl> getMovieForUser()
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(EnglishMovieImpl.class);
		List<EnglishMovieImpl> searchResult = crit.list();		
		return searchResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<EnglishMovieImpl> searchMovieByName ( String movieName )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(EnglishMovie.class);
		if(!StringUtils.isEmpty(movieName)){
			crit.add(Restrictions.like("name", "%" + movieName+"%"));
		}
		List<EnglishMovieImpl> searchResult = crit.list();		
		return searchResult;
	}
	
}
