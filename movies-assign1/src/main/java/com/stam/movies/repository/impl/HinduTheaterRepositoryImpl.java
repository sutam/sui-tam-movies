package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;

import com.stam.movies.entity.HinduTheater;
import com.stam.movies.entity.impl.HinduTheaterImpl;
import com.stam.movies.repository.HinduTheaterRepository;

@Repository
public class HinduTheaterRepositoryImpl implements HinduTheaterRepository{

	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param theater
	 * @return id of the newly created theater
	 */
	@Override
	public long addTheater(HinduTheater theater)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(theater);
	}
	
	public HinduTheater getTheater(long theaterId)
	{
		return (HinduTheater) this.sessionFactory.getCurrentSession().get(HinduTheaterImpl.class, theaterId);
	}
	
	@SuppressWarnings("unchecked")
	public List<HinduTheater> getTheaterForUser()
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(HinduTheater.class);
		List<HinduTheater> searchResult = crit.list();		
		return searchResult;
	}
	
	@SuppressWarnings("unchecked")
	public List<HinduTheater> searchTheaterByName ( String theaterName )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(HinduTheater.class);
		if(!StringUtils.isEmpty(theaterName)){
			crit.add(Restrictions.like("name", "%" + theaterName+"%"));
		}
		List<HinduTheater> searchResult = crit.list();		
		return searchResult;
	}
	
}
