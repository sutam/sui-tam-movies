package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;
import java.util.ArrayList;

import com.stam.movies.entity.EnglishMovie;
import com.stam.movies.entity.EnglishMovieSchedule;
import com.stam.movies.repository.EnglishScheduleRepository;
import com.stam.movies.entity.User;

@Repository
public class EnglishScheduleRepositoryImpl implements EnglishScheduleRepository{
	
	@Autowired
    private SessionFactory sessionFactory;
	
	public long addSchedule(EnglishMovieSchedule schedule)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(schedule);
	}
	
	@SuppressWarnings("unchecked")
	public List<EnglishMovieSchedule> searchScheduleByName(long theater, long movie )
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(EnglishMovieSchedule.class);
		crit.add(Restrictions.like("EnglishMovie_idMovie", "%" + movie +"%"));
		List<EnglishMovieSchedule> searchResult = crit.list();	
		List<EnglishMovieSchedule> result =  new ArrayList<>();
		for (int i = 0; i < searchResult.size(); i++)
		{
			EnglishMovieSchedule item = searchResult.get(i);
			if ( item.getidTheater() == theater )
				result.add(item);
		}
		return result;
	}
	
}
