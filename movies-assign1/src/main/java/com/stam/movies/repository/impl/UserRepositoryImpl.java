package com.stam.movies.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import java.util.List;
import com.stam.movies.repository.UserRepository;
import com.stam.movies.entity.impl.UserImpl;
import com.stam.movies.entity.User;

@Repository
public class UserRepositoryImpl implements UserRepository {
	
	@Autowired
    private SessionFactory sessionFactory;
	/**
	 * 
	 * @param user
	 * @return the id of the newly added user
	 */
	@Override
	public long addUser(User user)
	{
		return (Long) this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	public User getUser(long userId)
	{
		return (User) this.sessionFactory.getCurrentSession().get(UserImpl.class, userId);
	}
	
	@SuppressWarnings("unchecked")
	public List<User> search(String Username, String Password)
	{
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(User.class);
		if(!StringUtils.isEmpty(Username)){
			crit.add(Restrictions.like("user_name", "%"+Username+"%"));
		}
		if(!StringUtils.isEmpty(Password)){
			crit.add(Restrictions.like("password", "%"+Password+"%"));
		}
		List<User> searchResult = crit.list();		
		return searchResult;
	}

	public void update(User user)
	{
		this.sessionFactory.getCurrentSession().update(user);	
	}

}
