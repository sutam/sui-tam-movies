package com.stam.movies.service;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
	
import java.util.List;

import com.stam.movies.entity.*;
import com.stam.movies.entity.impl.*;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class TestMovieService extends AbstractJUnit4SpringContextTests   {	
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieService movieService;
	
	@Test
	public void addAndGetMovie(){	
		
		System.out.println("Running test addAndGetMovie ...");
		
		EnglishMovieImpl newMovie = new EnglishMovieImpl();
		newMovie.setName("Star Trek I");
		newMovie.setRating("PG 13");
		/*
		System.out.println("Calling moviesService.addMovie");
		Movie addedMovie = movieService.addMovie(newMovie);
		
		System.out.println("Validating movie id");
		Assert.assertNotEquals(0, addedMovie.getId());
		
		System.out.println("Validating movie name");
		Assert.assertEquals(newMovie.getName(), addedMovie.getName());
		
		Movie found = movieService.getMovie(addedMovie.getId());		
		Assert.assertEquals(addedMovie.getId(), found.getId());
		
		logger.info("added movie "+addedMovie);
		*/
	}
	
	/*
	@Test
	public void getAllMovies()
	{
		List<String> movies = movieService.getAllMovies();
		int i = 0;
		Assert.assertEquals( movies.get(i++), "Star Trek I" );
	}
		
	@Test
	public void getAllTheaters()
	{
		List<String> theaters = movieService.getAllTheaters();
		int i = 0;
		Assert.assertEquals( theaters.get(i++), "AMC1");
		
	}
		

	@Test
	public void getMovie(  )
	{
		List<EnglishMovieImpl> movie = movieService.getMovie( "Star Trek I");
		Assert.assertEquals( movie.get(0).getName(), "Star Trek I");
	}
	
	@Test
	public void getTheater( ){
		
		// TO DO:
		//  Get all the theater that play the movie
	}
	*/
}